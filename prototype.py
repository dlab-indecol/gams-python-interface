""" First prototype for the gams-gdx python interface

There are several possibilities to read GAMS data into python.

1. Reading GDX in directly in python (not used here)
=====================================================

Principally, all gams pyhton api calls need the keybindings installed in the
gams directory.  In addition, the packages are not updated regulary:

https://github.com/khaeru/py-gdx uses xarray
https://github.com/NREL/gdx-pandas uses pandas

2. Calling python in GAMS (not used here)
===========================================

https://www.gams.com/fileadmin/resources/presentations/informs2017_EmbeddedCode.pdf

3. Converting GDX to SQLITE (choosen solution)
==================================================

Currently, this seems the best solution.
It is quite easy (one step either in GAMS or subsequent in the command line or
in Python, using a tool which is shipped with GAMS).

    In GAMS:
        execute_unload 'result.gdx';
        execute 'gdx2sqlite -i result.gdx -o result.sqlite';

    Alternatively later in the command line
        gdx2sqlite -i result.gdx -o result.sqlite

SQLite data can be read and directly modfied in python with the sqlite library
and also in pandas.

I use the later here...

KST 20190125
"""
# standard library stuff
import logging
import os

# pandas
import pandas as pd

# sqlite pandas reading engine
from sqlalchemy import create_engine

# Plotting libraries:
# static plots
import matplotlib.pyplot as plt
import matplotlib

# interactive svg plots
import pygal as pg

# interactive html plots
import bokeh.plotting as bp


def read_data(sqlite_file):
    """ Returns data from sqlite db a pandas data frame
    """
    logging.info(f'Read data from {sqlite_file}')
    db = create_engine(f'sqlite:///{sqlite_file}')

    veh_stck_tot = pd.read_sql_table('VEH_STCK_TOT', db).set_index('year')
    veh_stck_tot.index = pd.to_datetime(veh_stck_tot.index)

    ice_stck_tot_check = pd.read_sql_table(
        'ICE_STCK_TOT_CHECK', db).set_index('year')
    bev_stck_tot_check = pd.read_sql_table(
        'BEV_STCK_TOT_CHECK', db).set_index('year')
    ice_stck_tot_check.index = pd.to_datetime(ice_stck_tot_check.index)
    bev_stck_tot_check.index = pd.to_datetime(bev_stck_tot_check.index)

    ice_bev = pd.concat([ice_stck_tot_check.level,
                         bev_stck_tot_check.level],
                        axis=1, keys=['ICE', 'BEV'])

    ice_bev.index = pd.to_datetime(ice_bev.index)

    return veh_stck_tot, ice_bev


def static_line_graph(df, title):
    """ Simple line graph example
    """
    # for available style see:
    # https://matplotlib.org/gallery/style_sheets/style_sheets_reference.html
    matplotlib.style.use('ggplot')
    # matplotlib.style.use('fivethirtyeight')

    df.plot(kind='line', title=title, linewidth=2)
    plt.tight_layout()
    plt.savefig(title + '.png', dpi=600)
    plt.close()
    # plt.show()


def static_stack_line_graph(df, title):
    """ Plot area graph for all columns in df
    """
    # for available style see:
    # https://matplotlib.org/gallery/style_sheets/style_sheets_reference.html
    # matplotlib.style.use('ggplot')
    matplotlib.style.use('fivethirtyeight')
    df.plot(kind='area', title='STACK')
    plt.tight_layout()
    plt.savefig(title + '.png', dpi=600)
    logging.info(f'Saved file to {os.getcwd() + "/" + title + ".png"}')
    plt.close()


def interactive_svg_line_plot(x_data, y_data, title):
    """ Using pgay for svg graph - open the result file in a browser
    """
    # interactive svg plots
    graph = pg.Line(x_label_rotation=45)
    graph.add(title, y_data)  # Add some values
    graph.x_labels = x_data
    graph.render_to_file(title + '.svg')
    logging.info(f'Saved file to {os.getcwd() + "/" + title + ".svg"}')


def interactive_svg_area_plot(df):
    """ Using pgay for svg graph - open the result file in a browser
    """

    area_graph = pg.StackedLine(x_label_rotation=45, fill=True)
    for col in df.columns:
        area_graph.add(col, df.loc[:, col])
    area_graph.x_labels = [entry.year for entry in df.index]
    file_name = 'and'.join(list(df.columns)) + '.svg'
    area_graph.render_to_file(file_name)
    logging.info(f'Saved file to {os.getcwd() + "/" + file_name}')


def interactive_html_plot(ice_bev):
    """ Just showing two line graphs
    """
    filename = 'ice_bev_tot.html'
    bp.output_file(filename, title='ICE and BEV plot')

    int_years = [entry.year for entry in ice_bev.index]

    ice_bev_html_graph = bp.figure(
        title="ICE and BEV data", x_axis_label='Years', y_axis_label='VALUE',
        tools="crosshair,pan,wheel_zoom,box_zoom,reset,hover")

    ice_bev_html_graph.line(int_years, ice_bev.ICE, legend="ICE",
                            line_width=0.2, color='olive')
    ice_bev_html_graph.circle(int_years, ice_bev.ICE, legend="ICE",
                              size=5, color='olive')
    ice_bev_html_graph.line(int_years, ice_bev.BEV, legend="BEV",
                            line_width=0.2, color='blue')
    ice_bev_html_graph.circle(int_years, ice_bev.BEV, legend="BEV",
                              size=5, color='blue')
    bp.save(ice_bev_html_graph)
    logging.info(f'Save interactive html at {os.getcwd()}/{filename}')
    # bp.show(ice_bev_html_graph)


def main():
    db_file = 'EVD4EUR_ALL_23Jan2019.sqlite'
    veh, ice_bev = read_data(db_file)

    # static plotting
    static_line_graph(veh, title='VEH_STCK_TOT')
    static_stack_line_graph(ice_bev, title='ICE_and_BEV')

    # Just for fun - some interactive plots
    interactive_svg_line_plot(x_data=[e.year for e in veh.index],
                              y_data=veh.value,
                              title='veh_line_plot')
    interactive_svg_area_plot(ice_bev)
    interactive_html_plot(ice_bev)

    return locals()


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    try:
        locals().update(main())
    except Exception as e:
        logging.exception(e)
        raise
